<h1>
Bluetooth Low Energy Ionic 4 + esp32 bike speedometer application
</h1>

Petro Mohyla Black Sea University
 
Rybchenko Serhii 305 group 

Speedometer was built speedometer by using Hall sensor on esp32 platform with application on Ionic 4 platform that are communicating by BLE (bluetooth low energy)   


<h2>
Main app page looks like
</h2>

![Alt text](pictures_for_readme/photo_2020-05-27_15-00-23.jpg "No file")


<h2>
Speedometer looks like
</h2>

![Alt text](pictures_for_readme/photo_2020-05-27_14-55-01.jpg "No file")


<h2>
EXAMPLE
</h2>

![Alt text](pictures_for_readme/ezgif-6-b450112ce266.gif "No file")

