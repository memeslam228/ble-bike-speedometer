import {Component, Input, NgZone} from '@angular/core';
import {AlertController, ModalController, NavParams, ToastController} from '@ionic/angular';
import {BLE} from '@ionic-native/ble/ngx';

@Component({
    selector: 'app-delails',
    templateUrl: './delails.page.html',
    styleUrls: ['./delails.page.scss'],
})
export class DelailsPage {

    @Input() device;

    peripheral: any = {};
    statusMessage = '';
    speed = 0;
    subSpeed = 0;
    failCounter = 0;
    interval;

    constructor(public navParams: NavParams,
                private modal: ModalController,
                private ble: BLE,
                private toastCtrl: ToastController,
                private ngZone: NgZone,
                private alert: AlertController) {

        const device = navParams.get('device');

        this.setStatus('');
        if (device !== null) {
            this.ble.connect(device.id).subscribe(
                peripheral => this.onConnected(peripheral),
                () => this.onDeviceDisconnected()
            );
        } else {
            setInterval(() => {
                if (this.subSpeed === 99) {
                    this.speed++;
                    this.subSpeed = 0;
                } else {
                    this.subSpeed++;
                }
            }, 50);
        }

    }

    async presentAlert() {
        const alert = await this.alert.create({
            header: 'Set up',
            subHeader: 'Setting',
            message: 'Enter the wheel size (in inches - " )',
            buttons: [
                {
                    text: 'Pass',
                    handler: (alertData) => {
                        this.sendWheelSize(alertData.size);
                    }
                }, {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: (alertData) => {
                        this.modal.dismiss();
                    }
                }
            ],
            inputs: [
                {
                    name: 'size',
                    type: 'number',
                    placeholder: '27.5'
                }
            ]
        });

        await alert.present();
    }

    sendWheelSize(size) {
        let array = new Uint8Array(size.length);
        for (let i = 0, l = size.length; i < l; i++) {
            array[i] = size.charCodeAt(i);
        }
        this.ble.write(this.peripheral.id,
            '4fafc201-1fb5-459e-8fcc-c5c9c331914b', 'beb5483e-36e1-4688-b7f5-ea07361b26a8', array.buffer)
            .then(data => {
                console.log(data);
                this.showAlert(data);
            }).catch(error => {
            this.showAlert(error);
            console.log(error);
        });
    }

    onConnected(peripheral) {
        this.peripheral = peripheral;
        this.setStatus('Connected to ' + (peripheral.name || peripheral.id));

        this.presentAlert();

        this.ble.startNotification(this.peripheral.id,
            '4fafc201-1fb5-459e-8fcc-c5c9c331914b', 'beb5483e-36e1-4688-b7f5-ea07361b26a8').subscribe(
            data => this.onButtonStateChange(data),
            (error) => {
                this.interval = setInterval(() => {
                    this.ble.read(this.peripheral.id,
                        '4fafc201-1fb5-459e-8fcc-c5c9c331914b', 'beb5483e-36e1-4688-b7f5-ea07361b26a8').then(data => {
                        this.failCounter = 0;
                        this.onButtonStateChange(data);
                    }).catch(errorRead => {
                        this.showAlert(errorRead);
                    });
                }, 100);
                this.showAlert('Failed to subscribe for button state changes');
            }
        );
    }

    onButtonStateChange(buffer: ArrayBuffer) {
        const data: any = new Uint8Array(buffer);

        this.ngZone.run(() => {
            this.speed = data[0];
            this.subSpeed = data[1];
        });

    }

    async onDeviceDisconnected() {
        const toast = await this.toastCtrl.create({
            message: 'The peripheral unexpectedly disconnected',
            duration: 3000,
            position: 'middle'
        });
        toast.present();
    }

    async showAlert(text: string) {
        this.failCounter++;
        if (this.failCounter > 5) {
            this.modal.dismiss();
        } else {
            const toast = await this.toastCtrl.create({
                message: text,
                duration: 1500,
                position: 'middle'
            });
            toast.present();
        }
    }

    // Disconnect peripheral when leaving the page
    ionViewWillLeave() {
        console.log('ionViewWillLeave disconnecting Bluetooth');
        this.ble.disconnect(this.peripheral.id).then(
            () => console.log('Disconnected ' + JSON.stringify(this.peripheral)),
            () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral))
        );
        clearInterval(this.interval);
    }

    setStatus(message) {
        console.log(message);
        this.ngZone.run(() => {
            this.statusMessage = message;
        });
    }


}
