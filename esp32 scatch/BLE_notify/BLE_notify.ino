#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

BLEServer* pServer = NULL;
BLECharacteristic* pCharacteristic = NULL;
bool deviceConnected = false;
bool oldDeviceConnected = false;
float WHEELCIRC = 0;
volatile byte revolutions;
float speed;
unsigned long timeold;
int rrradius = 29;
// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
      Serial.println("Connected");
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};

class MyCharacteristicCallback: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      float radius;
      std::string boof = pCharacteristic->getValue();
      Serial.println();
      Serial.println(boof.c_str());
      String boofer = boof.c_str();
      radius =  boofer.toFloat(); // inches
      Serial.println(radius);
      WHEELCIRC = 2 * PI * radius * 0.0254; // from meters
    }
};

void rpm_fun()
{
  if (deviceConnected) {
    revolutions++;
  }
}

void setup() {
  Serial.begin(115200);
  attachInterrupt(27, rpm_fun, FALLING);
  revolutions = 0;
  timeold = 0;
  speed = 0.0;
  speed = 0.0;


  WHEELCIRC = 2.0 * PI * rrradius * 0.0254; // from inches to meters

  // Create the BLE Device
  BLEDevice::init("ESP32 notify");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
                      CHARACTERISTIC_UUID,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_WRITE  |
                      BLECharacteristic::PROPERTY_NOTIFY |
                      BLECharacteristic::PROPERTY_INDICATE
                    );

  // https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.descriptor.gatt.client_characteristic_configuration.xml
  // Create a BLE Descriptor
  pCharacteristic->addDescriptor(new BLE2902());
  pCharacteristic->setCallbacks(new MyCharacteristicCallback());

  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(false);
  pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
  BLEDevice::startAdvertising();
  Serial.println("Waiting a client connection to notify...");
}

void loop() {
  // notify changed value
  if (deviceConnected) {
    if (revolutions >= 2) {
      speed = (revolutions * WHEELCIRC * 3600) / (millis() - timeold);
      timeold = millis();
      revolutions = 0;
    } else {
      if ((millis() - timeold) > 5000) {
        speed = 0.0;
      }
    }
    
    uint8_t speedData[2];
    speedData[0] = (int)speed;
    speedData[1] = (speed - (int)speed) * 100;
    pCharacteristic->setValue(speedData, 2);
    pCharacteristic->notify();
    delay(3); // bluetooth stack will go into congestion, if too many packets are sent, in 6 hours test i was able to go as low as 3ms
  }
  // disconnecting
  if (!deviceConnected && oldDeviceConnected) {
    revolutions = 0;
    timeold = 0;
    delay(500); // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising(); // restart advertising
    Serial.println("start advertising");
    oldDeviceConnected = deviceConnected;
  }
  // connecting
  if (deviceConnected && !oldDeviceConnected) {
    // do stuff here on connecting
    oldDeviceConnected = deviceConnected;
  }
  delay(10);
}
